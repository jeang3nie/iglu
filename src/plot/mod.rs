pub mod point;

use {
    clap::ArgMatches,
    crate::input::Input,
    point::Point,
    svg::{
        Document,
        node::element::{
            Path,
            path::Data,
        },
    },
};

pub struct Plot {
    input: Input,
    points: Vec<Point>,
}

impl Plot {
    pub fn new(matches: ArgMatches) -> Self {
        let input = Input::new(matches);
        let points = input.plot();
        Self {
            input,
            points,
        }
    }

    pub fn to_svg(&mut self) -> Document {
        let offset = self.input.offset();
        let range = self.input.range();
        let pt = self.points[0];
        let mut data = Data::new()
            .move_to((pt.x + offset, pt.y - self.input.low));
        for pt in &mut self.points[1..] {
            data = data.line_to((pt.x + offset, pt.y - self.input.low));
        }
        let path = Path::new()
            .set("fill", "none")
            .set("stroke", "black")
            .set("stroke-width", "0.01")
            .set("d", data);
        Document::new()
            .set("width", range)
            .set("height", range)
            .set("preserveAspectRatio", "xMidYMid meet")
            .add(path)
    }
}
