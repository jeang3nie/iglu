pub mod cli;
pub mod input;
pub mod plot;

use {
    cli::cli,
    plot::Plot,
};

fn main() {
    let matches = cli().get_matches();
    let mut plot = Plot::new(matches);
    let doc = plot.to_svg();
    println!("{}", doc);
}
