use clap::{Arg,Command};

pub fn cli() -> Command<'static> {
    Command::new("Cosh")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Nathan Fisher")
        .about("Plots a rough catenery curve")
        .arg(
            Arg::new("low")
                .help("The vertical location of the lowest point in the curve")
                .short('l')
                .long("low")
                .takes_value(true)
                .default_value("0")
        )
        .arg(
            Arg::new("start")
                .help("The horizontal starting location")
                .short('s')
                .long("start")
                .takes_value(true)
                .default_value("-10")
        )
        .arg(
            Arg::new("end")
                .help("The horizontal ending location")
                .short('e')
                .long("end")
                .takes_value(true)
                .default_value("10")
        )
        .arg(
            Arg::new("points")
                .help("The number of points to plot")
                .short('p')
                .long("points")
                .takes_value(true)
                .default_value("16")
        )
}
