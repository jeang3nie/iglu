use {
    clap::ArgMatches,
    crate::plot::point::Point,
};

pub struct Input {
    pub start: f64,
    pub end: f64,
    pub low: f64,
    pub points: usize,
}

impl Input {
    pub fn new(matches: ArgMatches) -> Self {
        Input {
            start: match matches.value_of_t("start") {
                Ok(c) => c,
                Err(e) => {
                    eprintln!("{}", e);
                    std::process::exit(1);
                },
            },
            end: match matches.value_of_t("end") {
                Ok(c) => c,
                Err(e) => {
                    eprintln!("{}", e);
                    std::process::exit(1);
                },
            },
            low: match matches.value_of_t("low") {
                Ok(c) => c,
                Err(e) => {
                    eprintln!("{}", e);
                    std::process::exit(1);
                },
            },
            points: match matches.value_of_t("points") {
                Ok(c) => c,
                Err(e) => {
                    eprintln!("{}", e);
                    std::process::exit(1);
                },
            },
        }
    }

    pub fn range(&self) -> f64 {
        match self.start {
            s if s < 0.0 => (s * -1.0) + self.end,
            s if s > 0.0 => self.end - s,
            s if s == 0.0 => self.end,
            _ => unreachable!(),
        }
    }

    pub fn offset(&self) -> f64 {
        if self.start < 0.0 {
            self.start * -1.0
        } else {
            0.0
        }
    }

    pub fn plot(&self) -> Vec<Point> {
        let mut plot = Vec::new();
        let range = self.range();
        let inc = range / (self.points - 1) as f64;
        for x in 0..self.points {
            let x = (x as f64 * inc) + self.start;
            let point = Point::new(x, self.low);
            plot.push(point);
        }
        plot
    }
}

















