Iglu takes user defined input and plots a
**[catenary](https://en.wikipedia.org/wiki/Catenary)** curve, outputting a simple
svg file. The output currently has simple to calculate straight lines between
points, which can easily be smoothed out via a good svg editor such as Inkscape.

## Nomenclature
Iglu is the Inuit spelling of the Inuit word, usually spelled phonetically in
modern English as *igloo*. The program is given the name because an iglu is
usually formed in the shape of a catenary arch in cross section.

## What is a catenary?
The catenary curve is the shape which an ideal chain or rope will assume when
suspended from two points under uniform gravity. When inverted, the catenary arch
is the most ideal arch in regards to perfectly supporting it's own weight against
the force of gravity.
